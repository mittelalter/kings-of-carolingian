# Summary

* [序](README.md)

* [第一卷 此身原是罗马人](chapter01/section1.md)

    * [第一章 起源于阿维尼亚](chapter01/section1.md)

    * [第二章 从南方到法兰克](chapter01/section2.md)

    * [第三章 最强教会为梅斯](chapter01/section3.md)

    * [第四章 一统高卢墨洛温](chapter01/section4.md)

    * [第五章 同室操戈四十载](chapter01/section5.md)

    * [第六章 王族屠杀何时休](chapter01/section6.md)

    * [第七章 懒王时代在接近](chapter01/section7.md)

    * [第八章 阿努尔夫的传说](chapter01/section8.md)

    * [第九章 话回往昔探宫相](chapter01/section9.md)

    * [第十章 关系良好老丕平](chapter01/section10.md)

* [第二卷 曹操们的角斗场](chapter02/section1.md)

    * [第一章 日薄西山王权衰](chapter02/section1.md)

    * [第二章 兰登家族的儿女](chapter02/section2.md)

    * [第三章 墨洛温王皆懒王](chapter02/section3.md)

    * [第四章 成王梦碎家族灭](chapter02/section4.md)

    * [第五章 法兰克中有三杰](chapter02/section5.md)

    * [第六章 跋扈之人起刀戈](chapter02/section6.md)

    * [第七章 专横之世舞战旗](chapter02/section7.md)

    * [第八章 豚奔犬窜小丕平](chapter02/section8.md)

    * [第九章 横扫高卢无敌手](chapter02/section9.md)

    * [第十章 阿尔萨斯的番外](chapter02/section10.md)

* [第三卷 险夭折而凤涅槃](chapter03/section1.md)

    * [第一章 稳固东方的努力](chapter03/section1.md)

    * [第二章 阿勒曼尼亚往事](chapter03/section2.md)

    * [第三章 小丕平的垂暮年](chapter03/section3.md)

    * [第四章 墨洛温绝地求生](chapter03/section4.md)

    * [第五章 升天堕地两相分](chapter03/section5.md)

    * [第六章 锋芒渐露初磨刀](chapter03/section6.md)

    * [第七章 脚踩教会任米洛](chapter03/section7.md)

    * [第八章 横扫敌手无人制](chapter03/section8.md)

    * [第九章 东部边疆战云起](chapter03/section9.md)

    * [第十章 东部敌酋尽俯首](chapter03/section10.md)

* [第四卷 雄日朝升定基业](chapter04/section1.md)

    * [第一章 南域野狼朝北望](chapter04/section1.md)

    * [第二章 心有灵犀双夹击](chapter04/section2.md)

    * [第三章 南方星月蔽庭野](chapter04/section3.md)

    * [第四章 新月十字战图尔](chapter04/section4.md)

    * [第五章 波动千载说不停](chapter04/section5.md)

    * [第六章 威震四方万人畏](chapter04/section6.md)

    * [第七章 北域来客马蹄疾](chapter04/section7.md)

    * [第八章 南域之军败加败](chapter04/section8.md)

    * [第九章 没有国王的王国](chapter04/section9.md)

    * [第十章 斯人已逝业成空](chapter04/section10.md)

* [第五卷 旧王亡矣新王登](chapter05/section1.md)

    * [第一章 泰山崩而天柱塌](chapter05/section1.md)

    * [第二章 东征西讨叛军平](chapter05/section2.md)

    * [第三章 十字蒙尘望拂去](chapter05/section3.md)

    * [第四章 沉舟侧畔丕平笑](chapter05/section4.md)

    * [第五章 受寿永多夫何长](chapter05/section5.md)

    * [第六章 七年不落纳博纳](chapter05/section6.md)

    * [第七章 郎有情而妾有意](chapter05/section7.md)

    * [第八章 斗转星移仍不落](chapter05/section8.md)

    * [第九章 献土破城备清算](chapter05/section9.md)

    * [第十章 奠基者去霸者来](chapter05/section10.md)

* [第六卷 查理大帝争南域](chapter06/section1.md)

    * [第一章 初登大位南域乱](chapter06/section1.md)

    * [第二章 亲妈调解亲弟哭](chapter06/section2.md)

    * [第三章 手足离心争端起](chapter06/section3.md)

    * [第四章 叔侄相争战火燃](chapter06/section4.md)

    * [第五章 南方有国伦巴第](chapter06/section5.md)

    * [第六章 螳螂捕蝉雀在后](chapter06/section6.md)

    * [第七章 历史总是在轮回](chapter06/section7.md)

    * [第八章 后人哀之而不鉴](chapter06/section8.md)

    * [第九章 贤王逝后庸王频](chapter06/section9.md)

    * [第十章 头戴伦巴第铁冠](chapter06/section10.md)

* [第七卷 东侵南伐战不休](chapter07/section1.md)

    * [第一章 卅年血战由此始](chapter07/section1.md)

    * [第二章 图腾倒下战火燃](chapter07/section2.md)

    * [第三章 三大氏族皆俯首](chapter07/section3.md)

    * [第四章 教皇密信说叛逆](chapter07/section4.md)

    * [第五章 南方忠臣诓援军](chapter07/section5.md)

    * [第六章 雄才大略诓被诓](chapter07/section6.md)

    * [第七章 血染荆棘遭惨败](chapter07/section7.md)

    * [第八章 狼狈退军心难定](chapter07/section8.md)

    * [第九章 血染东域人头滚](chapter07/section9.md)

    * [第十章 千年回首韦登居](chapter07/section10.md)

* [第八卷 南蛮敌酋俱不敌](chapter08/section1.md)

    * [第一章 南讨僭臣终前事](chapter08/section1.md)

    * [第二章 海上王子欲返乡](chapter08/section2.md)

    * [第三章 东镇逆臣翻旧账](chapter08/section3.md)

    * [第四章 磨刀洗俎宰鱼肉](chapter08/section4.md)

    * [第五章 东南扩土无枯骨](chapter08/section5.md)

    * [第六章 潘诺尼亚马蹄疾](chapter08/section6.md)

    * [第七章 血色宫廷欲谋乱](chapter08/section7.md)

    * [第八章 圣人辅政阿基坦](chapter08/section8.md)

    * [第九章 比利牛斯战云扬](chapter08/section9.md)

    * [第十章 草原蛮族势微衰](chapter08/section10.md)

* [第九卷 人去业存照千年](chapter9/section1.md)

    * [第一章 河东蛮族两相争](chapter9/section1.md)

    * [第二章 奥古斯都查理曼](chapter9/section2.md)

    * [第三章 帝国存续法兰克](chapter9/section3.md)

    * [第四章 两个帝国的交锋](chapter9/section4.md)

    * [第五章 三个儿子的安排](chapter9/section5.md)

    * [第六章 维京边患尚待中](chapter9/section6.md)

    * [第七章 人生自古谁无死](chapter9/section7.md)

    * [第八章 文治武功皆显赫](chapter9/section8.md)

    * [第九章 君王亦有柴油事](chapter9/section9.md)

    * [第十章 霸主也有柔情处](chapter9/section10.md)

* [第十卷 承父伟业步履艰](chapter10/section1.md)

    * [第一章 幺子未料能成王](chapter10/section1.md)

    * [第二章 一朝天子一朝臣](chapter10/section2.md)

    * [第三章 新君欲更万象事](chapter10/section3.md)

    * [第四章 心忧人暮效父策](chapter10/section4.md)

    * [第五章 雷霆手段镇叛逆](chapter10/section5.md)

    * [第六章 惩戒宽恕转瞬间](chapter10/section6.md)

    * [第七章 边防呈现末世兆](chapter10/section7.md)

    * [第八章 续弦得子战端始](chapter10/section8.md)

    * [第九章 教皇国内双权斗](chapter10/section9.md)

    * [第十章 罗马宪章制教皇](chapter10/section10.md)

* [第十一卷 父子相残国日蹙](chapter11/section1.md)

    * [第一章 南域再现荆棘谷](chapter11/section1.md)

    * [第二章 镇叛得力进宫廷](chapter11/section2.md)

    * [第三章 疲病渐生心憔悴](chapter11/section3.md)

    * [第四章 为了皇帝叛皇帝](chapter11/section4.md)

    * [第五章 姜的还是老的辣](chapter11/section5.md)

    * [第六章 心有怨惧再叛父](chapter11/section6.md)

    * [第七章 谎言之地臣皆叛](chapter11/section7.md)

    * [第八章 困龙升天再斗战](chapter11/section8.md)

    * [第九章 依理建言终内战](chapter11/section9.md)

    * [第十章 帝崩国乱已黄昏](chapter11/section10.md)

* [第十二卷 兄弟阋墙日黄昏](chapter12/section1.md)

    * [第一章 人逝乱存战未消](chapter12/section1.md)

    * [第二章 丰特努瓦四王战](chapter12/section2.md)

    * [第三章 战败力孤难回天](chapter12/section3.md)

    * [第四章 帝国三分凡尔登](chapter12/section4.md)

    * [第五章 心怨再战阿基坦](chapter12/section5.md)

    * [第六章 绕约惑人撬弟国](chapter12/section6.md)

    * [第七章 北境狼烟叛臣燃](chapter12/section7.md)

    * [第八章 血染塞纳河畔口](chapter12/section8.md)

    * [第九章 北败南乱国难平](chapter12/section9.md)

    * [第十章 一人欢喜二人哭](chapter12/section10.md)

* [第十三卷 中逝只余东西法](chapter13/section1.md)

    * [第一章 辞旧迎新万象替](chapter13/section1.md)

    * [第二章 萨拉森人南海来](chapter13/section2.md)

    * [第三章 秃头屡战又屡败](chapter13/section3.md)

    * [第四章 中法兰克再三分](chapter13/section4.md)

    * [第五章 皇崩内战旋即起](chapter13/section5.md)

    * [第六章 来也匆匆去也匆](chapter13/section6.md)

    * [第七章 丈夫欲休无子妻](chapter13/section7.md)

    * [第八章 黎明前夕身先死](chapter13/section8.md)

    * [第九章 洛林二分梅尔森](chapter13/section9.md)

    * [第十章 有为奈何无子逝](chapter13/section10.md)
* [第十四卷 一年二王相辞去](chapter14/section1.md)

   * [第一章 东法统治卅年间](chapter14/section1.md)

   * [第二章 天道轮回父子残](chapter14/section2.md)

   * [第三章 煮熟鸭子被窃取](chapter14/section3.md)

   * [第四章 有成有失又战败](chapter14/section4.md)

   * [第五章 弗兰德斯的姻缘](chapter14/section5.md)

   * [第六章 这是传统要理解](chapter14/section6.md)

   * [第七章 拳殴黑心贼叔叔](chapter14/section7.md)

   * [第八章 战后东法亦三分](chapter14/section8.md)

   * [第九章 裙带上位野心家](chapter14/section9.md)

   * [第十章 命陨南征铁冠换](chapter14/section10.md)

* [第十五卷 破镜重圆不复昔](chapter15/section1.md)

   * [第一章 懦弱软耳不肖王](chapter15/section1.md)

   * [第二章 战狂迟暮忧后事](chapter15/section2.md)

   * [第三章 坑父坑子真弟弟](chapter15/section3.md)

   * [第四章 吃瓜群众乐呵呵](chapter15/section4.md)

   * [第五章 逼侄割土吃绝户](chapter15/section5.md)

   * [第六章 三王合力战维京](chapter15/section6.md)

   * [第七章 四王合力讨不臣](chapter15/section7.md)

   * [第九章 索科特再战维京](chapter15/section9.md)

   * [第十章 重一统鸿运当头](chapter15/section10.md)

* [第十六卷 覆水难收况庸主](chapter16/section1.md)

   * [第一章 孝子拳把老父打](chapter16/section1.md)

   * [第二章 听谗放盗威信散](chapter16/section2.md)

   * [第三章 勇将御盗斩叛臣](chapter16/section3.md)

   * [第四章 南域贵族跋扈事](chapter16/section4.md)

   * [第五章 边境不稳枭雄侵](chapter16/section5.md)

   * [第六章 叔父教父皆失和](chapter16/section6.md)

   * [第七章 维京再染塞纳河](chapter16/section7.md)

   * [第八章 纵匪失威人心散](chapter16/section8.md)

   * [第九章 与妻离婚求子正](chapter16/section9.md)

   * [第十章 一朝身死国五分](chapter16/section10.md)

* [第十七卷 壮志未成身先死](chapter17/section1.md)

   * [第一章 夺洛林应归子地](chapter17/section1.md)

   * [第二章 铁王冠易手他姓](chapter17/section2.md)

   * [第三章 双雄对峙意大利](chapter17/section3.md)

   * [第四章 王冠重落遗腹子](chapter17/section4.md)

   * [第五章 鲁汶维京尸填河](chapter17/section5.md)

   * [第六章 旧人崩离新人来](chapter17/section6.md)

   * [第七章 南域交锋未全功](chapter17/section7.md)

   * [第八章 进军罗马称皇帝](chapter17/section8.md)

   * [第九章 行百里者九十跌](chapter17/section9.md)

   * [第十章 中道崩殂应曰悼](chapter17/section10.md)

* [第十八卷 游牧踏碎东法境](chapter18/section1.md)

   * [第一章 洛林归给私生子](chapter18/section1.md)

   * [第二章 四面楚歌身亦陨](chapter18/section2.md)

   * [第三章 东法兰克诸贵族](chapter18/section3.md)

   * [第四章 布伦塔南国丧胆](chapter18/section4.md)

   * [第五章 盟约破东法遭劫](chapter18/section5.md)

   * [第六章 下决心出兵讨伐](chapter18/section6.md)

   * [第七章 三军葬雷根斯堡](chapter18/section7.md)

   * [第八章 难忍劫掠再发兵](chapter18/section8.md)

   * [第九章 奥格斯堡再惨败](chapter18/section9.md)

   * [第十章 心郁身死国祚终](chapter18/section10.md)

* [第十九卷 滚滚塞纳东逝水](chapter19/section1.md)

   * [第一章 步行者来诺曼底](chapter19/section1.md)

   * [第二章 偏颇识人刀兵起](chapter19/section2.md)

   * [第三章 一死一败一捡漏](chapter19/section3.md)

   * [第四章 三法王族皆易姓](chapter19/section4.md)

   * [第五章 我也不是要谦虚](chapter19/section5.md)

   * [第六章 海上归来的王子](chapter19/section6.md)

   * [第七章 插手洛林欲扩土](chapter19/section7.md)

   * [第八章 扩土不成被吊打](chapter19/section8.md)

   * [第九章 臣死遂动却遭囚](chapter19/section9.md)

   * [第十章 王朝末年因果循](chapter19/section10.md)

* [第二十卷 浪花淘尽加洛林](chapter20/section1.md)

   * [第一章 拙计却反成鸩药](chapter20/section1.md)

   * [第二章 西法兰克三摄政](chapter20/section2.md)

   * [第三章 摄政治下无大事](chapter20/section3.md)

   * [第四章 大帝逝后东西争](chapter20/section4.md)

   * [第五章 你来我往回合战](chapter20/section5.md)

   * [第六章 少夫老妻失协调](chapter20/section6.md)

   * [第七章 神罗内乱夺洛林](chapter20/section7.md)

   * [第八章 先定一个小目标](chapter20/section8.md)

   * [第九章 王位非自有生来](chapter20/section9.md)

   * [第十章 查理伟业已成梦](chapter20/section10.md)
